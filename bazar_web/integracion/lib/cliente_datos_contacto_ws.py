import requests
import json

def guardar (datosContactoJson):
    try:
        url='http://127.0.0.1:9000/api/v1/contacto'

        response=requests.post(url,data=datosContactoJson)

        codigo=response.json()['codigo']
        descripcion=response.json()['descripcion']

        print('RESPONSE:{0}'.format(response.json()))

        if codigo==1:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        print('ERROR: Datos Contacto no fue almecenado.')
        return False

def buscarTodo():
    try:
        url='http://127.0.0.1:9000/api/v1/contacto'

        response=requests.get(url)

        codigo=response.json()['codigo']

        print('RESPONSE:{0}'.format(response.json()))

        if codigo==1:
            return  response.json()['datos']
        else:
            return []
    except Exception as e:
        print(e)
        print('ERROR: problemas con el servicio.')
        return []

def eliminar(id):
    try:
        url='http://127.0.0.1:9000/api/v1/contacto'
        response= requests.delete(url, data={'id':id})
        codigo=response.json()['codigo']

        print('RESPONSE:{0}'.format(response.json()))

        if codigo==1:
            return  True
        else:
            return False
    except Exception as e:
        print(e)
        print('ERROR: problemas con el servicio.')
        return False

def buscar_por_id(id):
    try:
        url = 'http://127.0.0.1:9000/api/v1/contacto/{0}'.format(id)
        response = requests.get(url)

        codigo = response.json()['codigo']
        print('RESPONSE: {0}'.format(response.json()))

        if codigo == 1:
            return response.json()['dato']
        else:
            return []
    except Exception as e:
        print(e)
        print('ERROR: Problemas con el servicio')
        return []

def actualizar(datosContactoJson):
    try:
        url = 'http://127.0.0.1:9000/api/v1/contacto'
        print('actualizando servicio -> {0}'.format(url))
        response = requests.put(url, data=datosContactoJson)
        
        codigo = response.json()['codigo']
        print('RESPONSE: {0}'.format(response.json()))

        if codigo == 1:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        print('ERROR: Problemas con el servicio')
        return False
 