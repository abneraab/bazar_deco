from django.http import HttpResponse,HttpResponseRedirect
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from persistencia.models import DatosContacto
from integracion.lib.cliente_datos_contacto_ws import guardar,buscarTodo,eliminar,buscar_por_id, actualizar
from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout


def home(request): 
    usuario = request.user.username
    return render(request, "index.html",{'username':usuario} )

def catalogo(request):    
    usuario = request.user.username
    return render(request, "catalogo.html",{'username':usuario} )

def contact(request):   
    usuario = request.user.username
    return render(request, "contact.html",{'username':usuario} )    

def registrar_contacto(request):
    usuario = request.user.username
    message_error = ''
    message_success = ''
    if request.method == 'POST': 

        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        email = request.POST['email']
        telefono = request.POST['telefono']
        asunto = request.POST['asunto']
        datosContactoJson = {
                                'nombres': nombres,
                                'apellido-paterno': apellido_paterno,
                                'apellido-materno': apellido_materno,
                                'email': email,
                                'telefono': telefono,
                                'asunto': asunto,
                            }
        resultado = guardar(datosContactoJson)
        if resultado:
            message_success = 'DATOS GUARDADOS.'
        else:
            message_error = 'ERROR AL GUARDAR EL REGISTRO.'
    else:
        print('METODO NO SOPORTADO.')
    return render(request, "contact.html", 
        {'message_error': message_error, 'message_success': message_success,'username':usuario })


def subject(request):
    usuario = request.user.username
    if request.user.is_authenticated:
       datosContacto= buscarTodo()

       for dato in datosContacto:
        print(dato)

       return render(request,"subject.html",{'datosContactos': datosContacto ,'username':usuario })
    else:
        print("redirecionando a home")
        return HttpResponseRedirect('/home')

def eliminar_contacto(request):
    usuario = request.user.username
    if request.method == 'GET': 

        id = request.GET['id']
        resultado = eliminar(id)

        if resultado:
            print('REGISTRO ELIMINADO CORRECTAMENTE.')
        
        else:
            print('REGISTRO NO SE PUDO ELIMINAR.')
    else:
        print('MÉTODO NO SOPORTADO') 

    datosContactos = buscarTodo()
    return render(request, "subject.html",  {'datosContactos': datosContactos,'username':usuario} )

def form_editar_contacto(request):
    usuario = request.user.username
    resultado = []
    if request.method == 'GET': 
        id = request.GET['id']
        resultado = buscar_por_id(id)

    return render(request, "edit-contact.html", {'contacto': resultado,'username':usuario} )

def actualizar_contacto(request):
    usuario = request.user.username
    message_error = ''
    message_success = ''
    if request.method == 'POST': 
        id = request.POST['id']
        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        email = request.POST['email']
        telefono = request.POST['telefono']
        asunto = request.POST['asunto']
        datosContactoJson = {
                                'id': id,
                                'nombres': nombres,
                                'apellido-paterno': apellido_paterno,
                                'apellido-materno': apellido_materno,
                                'email': email,
                                'telefono': telefono,
                                'asunto': asunto,
                            }
        resultado = actualizar(datosContactoJson)
        print('RESULTADO: {0}'.format(resultado))
        if resultado:
            message_success = 'DATOS ACTUALIZADOS.'
        else:
            message_error = 'ERROR AL ACTUALIZAR EL REGISTRO.'
    else:
        print('METODO NO SOPORTADO.')
    return render(request, "edit-contact.html", 
        {'message_error': message_error, 'message_success': message_success,'username':usuario} )

def autenticar(request):
    usuario = request.user.username
    usuario = []
    if request.method == 'POST': 
        usuario = request.POST['usuario']
        password = request.POST['password']

        user = authenticate(username=usuario, password=password)

        if user is not None:
            print('AUTENTICACIÓN CORRECTA')           
            do_login(request, user)
        else:
            print('USUARIO O CONTRASEÑAS INCORRECTOS')           
            
    return render(request, "index.html", {'username': usuario,'username':usuario} )

def logout(request):
    do_logout(request)
    return render(request, "index.html")